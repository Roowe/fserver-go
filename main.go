package main

import (
	"bytes"
	"flag"
	"fmt"
	. "fserver/utils"
	"io"
	"net/http"
	"os"
)

func usage() {
	fmt.Fprintf(os.Stderr, "usage: %s\n", os.Args[0])
	flag.PrintDefaults()
	os.Exit(1)
}

var VERSION string
var laddr string

func main() {
	version := flag.Bool("v", false, "Get version info")
	debug := flag.Bool("debug", false, "print debug info")
	flag.StringVar(&DataFile, "data", "data/data.json", "data file")
	flag.StringVar(&laddr, "listen", ":8089", "listen address")
	flag.StringVar(&Domain, "domain", "127.0.0.1:8089", "domain address") // 统一下单使用
	flag.Usage = usage
	flag.Parse()

	if *version {
		fmt.Printf("Version: %s\n", VERSION)
		os.Exit(1)
	}

	InitLogger(*debug)
	InitDb()
	defer DbConn.Close()
	if _, err := os.Stat(DataFile); err != nil {
		Logger.Errorf("%s", err)
		os.Exit(1)
	}
	http.HandleFunc("/auth/fake/login", handleWrapper(AuthFakeLoginHandle))
	http.HandleFunc("/auth/fake/acc/list", handleWrapper(AuthFakeAccListHandle))
	http.HandleFunc("/sns/oauth2/access_token", handleWrapper(SnsOauth2AccessTokenHandle))
	http.HandleFunc("/sns/userinfo", handleWrapper(SnsUserinfoHandle))
	http.HandleFunc("/connect/oauth2/authorize", handleWrapper(ConnectOauth2AuthHandle))
	http.HandleFunc("/pay/unifiedorder", handleWrapper(PayUnifiedorderHandle))
	http.HandleFunc("/payment/confirmation", handleWrapper(PaymentConfirmationHandle))
	http.HandleFunc("/payment/authorization", handleWrapper(PaymentAuthorizationHandle))
	http.HandleFunc("/test/pay_notify", handleWrapper(PayCallbackHandle))
	http.HandleFunc("/mmpaymkttransfers/promotion/transfers", handleWrapper(DoTransfers))
	http.HandleFunc("/mmpaymkttransfers/gettransferinfo", handleWrapper(GetTransfers))
	Logger.Infof("starting server, listen %s", laddr)
	Logger.Fatal(http.ListenAndServe(laddr, nil))
}

func handleWrapper(f func(io.Writer, *http.Request) error) func(http.ResponseWriter, *http.Request) {
	return func(rw http.ResponseWriter, r *http.Request) {
		Logger.Debugf("%s %s", r.URL.Path, r.URL.RawQuery)
		w := bytes.NewBuffer(nil)
		err := f(w, r)
		if err != nil {
			http.Error(rw, fmt.Sprintf("Internal server error: %s", err), http.StatusInternalServerError)
		} else {
			rw.Write(w.Bytes())
		}
	}
}
