package utils

var Domain string

const (
	EC_ERROR_PARAMS = 1000 // 参数错误
	EC_ERROR_ACC_ID = 1001 // 错误的acc id
	EC_ERROR_JSON   = 1002 // 错误的JSON格式
	EC_ERROR_APP_ID = 1003 // 错误的app id
	EC_ERROR_SECRET = 1004 // 错误的secret
	EC_ERROR_CODE   = 1005 // 错误的code
)

const (
	MSG_ERROR_PARAMS         = "参数错误"
	MSG_ERROR_ACC_ID         = "错误的acc id"
	MSG_ERROR_JSON           = "错误的JSON格式"
	MSG_ERROR_APP_ID         = "错误的app id"
	MSG_ERROR_SECRET         = "错误的secret"
	MSG_ERROR_CODE           = "错误的code"
	MSG_ERROR_PAYMENT_CLOSED = "订单已关闭"
)
