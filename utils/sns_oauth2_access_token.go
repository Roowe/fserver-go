package utils

import (
	"encoding/json"
	"io"
	"net/http"
)

// http://127.0.0.1:8089/sns/oauth2/access_token?appid=app_id_1&secret=secret&code=code&grant_type=authorization_code
func SnsOauth2AccessTokenHandle(w io.Writer, req *http.Request) error {
	appId := req.URL.Query().Get("appid")
	secret := req.URL.Query().Get("secret")
	code := req.URL.Query().Get("code")

	appData, found := findAppData(appId)
	var accRet Account
	var ret int
	if found {
		if appData.Secret != secret {
			ret = EC_ERROR_SECRET
		} else {
			ret = EC_ERROR_CODE
			for _, acc := range appData.AccInfos {
				if acc.Code == code {
					accRet = acc
					ret = 0
					break
				}
			}
		}
	} else {
		ret = EC_ERROR_APP_ID
	}

	data := make(map[string]interface{})

	if ret != 0 {
		data["ret"] = ret
	} else {
		data["access_token"] = accRet.AccessToken
		data["openid"] = accRet.OpenId
		data["unionid"] = accRet.UnionId
	}
	reply, _ := json.Marshal(data)
	w.Write(reply)
	return nil
}
