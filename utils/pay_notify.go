package utils

import (
	"bytes"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"time"
)

var sleepSec = []time.Duration{15, 15, 30, 180, 1800, 1800, 1800, 1800, 3600}

type PayNotifyAck struct {
	ReturnCode string `xml:"return_code"`
}

func payNotifyLoop(URL string, body []byte) {
	c := 0
	Logger.Debugf("%s %s %d", URL, body, c)
	for c := 0; c < len(sleepSec); c++ {
		resp, err := http.Post(URL, "text/xml", bytes.NewReader(body))
		if err == nil {
			recvBody, err := ioutil.ReadAll(resp.Body)
			resp.Body.Close()
			var v PayNotifyAck
			err = xml.Unmarshal(recvBody, &v)
			if err == nil && v.ReturnCode == "SUCCESS" {
				// stop
				Logger.Debug("pay_notify success")
				return
			}
		}
		Logger.Debug("pay_notify fail, will retry")
		time.Sleep(sleepSec[c] * time.Second)
	}
}
