package utils

import (
	"github.com/boltdb/bolt"
)

const (
	mmpaymkttransfersBucket = "mmpaymkttransfers"
)

var DbConn *bolt.DB

func InitDb() {
	db, err := bolt.Open("fserver.db", 0600, nil)
	if err != nil {
		Logger.Fatal(err)
	}
	DbConn = db // 设置全局db
	err = DbConn.Update(func(tx *bolt.Tx) error {
		_, cerr := tx.CreateBucketIfNotExists([]byte(mmpaymkttransfersBucket))
		if cerr != nil {
			return cerr
		}
		return nil
	})
	if err != nil {
		Logger.Fatal(err)
	}
}
