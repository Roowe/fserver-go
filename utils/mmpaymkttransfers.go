package utils

import (
	"bytes"
	"encoding/gob"
	"encoding/xml"
	"errors"
	"github.com/boltdb/bolt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"time"
)

var mmpaymktCodeDesc = map[string]string{
	"NO_AUTH":               "没有该接口权限",
	"PARAM_ERROR":           "参数错误",
	"OPENID_ERROR":          "Openid错误",
	"SEND_FAILED":           "付款错误",
	"NOTENOUGH":             "余额不足",
	"SYSTEMERROR":           "系统繁忙，请稍后再试。",
	"NAME_MISMATCH":         "姓名校验出错",
	"SIGN_ERROR":            "签名错误",
	"XML_ERROR":             "Post内容出错",
	"FATAL_ERROR":           "两次请求参数不一致",
	"FREQ_LIMIT":            "超过频率限制，请稍后再试。",
	"MONEY_LIMIT":           "已经达到今日付款总额上限/已达到付款给此用户额度上限",
	"CA_ERROR":              "证书出错",
	"V2_ACCOUNT_SIMPLE_BAN": "无法给非实名用户付款",
	"PARAM_IS_NOT_UTF8":     "请求参数中包含非utf8编码字符",
	"AMOUNT_LIMIT":          "付款失败，因你已违反《微信支付商户平台使用协议》，单笔单次付款下限已被调整为5元",
	"REQUIRE_POST_METHOD":   "请使用post方法",
	"APPID_NOT_EXIST":       "APPID不存在",
	"MCHID_NOT_EXIST":       "MCHID不存在",
	"APPID_MCHID_NOT_MATCH": "appid和mch_id不匹配",
}

type DoTransReq struct {
	AppId          string `xml:"mch_appid" len:"32"`
	MCHId          string `xml:"mchid" len:"32"`
	DeviceInfo     string `xml:"device_info" len:"32" opt:"1"`
	NonceStr       string `xml:"nonce_str" len:"32"`
	Sign           string `xml:"sign" len:"32"`
	PartnerTradeNo string `xml:"partner_trade_no" len:"32"`
	OpenId         string `xml:"openid" len:"128"`
	CheckName      string `xml:"check_name" len:"128"`
	ReUserName     string `xml:"re_user_name" len:"16" opt:"1"`
	Amount         int    `xml:"amount" len:"128"`
	Desc           string `xml:"desc" len:"128"`
	SpBillCreateIp string `xml:"spbill_create_ip" len:"32"`
}
type PayRet struct {
	ReturnCode *CdataString `xml:"return_code,omitempty"`
	ReturnMsg  *CdataString `xml:"return_msg,omitempty"`
}

func NewPayRet(msgCode string) *PayRet {
	if msgCode == "SUCCESS" {
		return &PayRet{
			ReturnCode: NewCdataString("SUCCESS"),
		}
	} else {
		return &PayRet{
			ReturnCode: NewCdataString("FAIL"),
			ReturnMsg:  NewCdataString(mmpaymktCodeDesc[msgCode]),
		}
	}
}

type PayRes struct {
	ResultCode *CdataString `xml:"result_code,omitempty"`
	ErrCode    *CdataString `xml:"err_code,omitempty"`
	ErrCodeDes *CdataString `xml:"err_code_des,omitempty"`
}

func NewPayRes(msgCode string) *PayRes {
	if msgCode == "SUCCESS" {
		return &PayRes{
			ResultCode: NewCdataString("SUCCESS"),
		}
	} else {
		return &PayRes{
			ResultCode: NewCdataString("FAIL"),
			ErrCode:    NewCdataString(msgCode),
			ErrCodeDes: NewCdataString(mmpaymktCodeDesc[msgCode]),
		}
	}
}

type DoTransAck struct {
	XMLName xml.Name `xml:"xml"`
	PayRet
	AppId      *CdataString `xml:"mch_appid,omitempty"`
	MCHId      *CdataString `xml:"mchid,omitempty"`
	DeviceInfo *CdataString `xml:"device_info,omitempty"`
	NonceStr   *CdataString `xml:"nonce_str,omitempty"`
	PayRes
	PartnerTradeNo *CdataString `xml:"partner_trade_no,omitempty"`
	PaymentNo      *CdataString `xml:"payment_no,omitempty"`
	PaymentTime    *CdataString `xml:"payment_time,omitempty"`
}

type TransData struct {
	Req *DoTransReq
	Ack *DoTransAck
}

func (v *DoTransReq) returnCode() (*AppData, *PayRet) {
	if payRet := v.verifyFieldType(); payRet != nil {
		return nil, payRet
	}
	appData, found := findAppData(v.AppId)
	if !found {
		return nil, NewPayRet("APPID_NOT_EXIST")
	}
	kvs := toMap(v)
	calcSign := wxSign(kvs, appData.MCHSecret)
	if calcSign != v.Sign {
		Logger.Debugf("client_sign %s", v.Sign)
		Logger.Debugf("server_sign %s", calcSign)
		return nil, NewPayRet("SIGN_ERROR")
	}
	return &appData, nil
}
func (v *DoTransReq) resultCode(appData *AppData) *PayRes {
	if appData.MCHId != v.MCHId {
		return NewPayRes("MCHID_NOT_EXIST")
	}

	if m, _ := regexp.MatchString("^[a-zA-Z0-9_-]{0,32}$", v.PartnerTradeNo); !m {
		return NewPayRes("PARAM_ERROR")
	}
	var payRes *PayRes
	key := keyGen(v.AppId, v.MCHId, v.PartnerTradeNo)
	err := DbConn.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(mmpaymkttransfersBucket))
		transDataBytes := b.Get([]byte(key))
		if transDataBytes != nil {
			Logger.Warningf("duplicate PartnerTradeNo %v", v.PartnerTradeNo)
			payRes = NewPayRes("PARAM_ERROR")
		}
		return nil
	})
	if err != nil {
		return NewPayRes("PARAM_ERROR")
	}
	return payRes
}

func (v *DoTransReq) verifyFieldType() *PayRet {
	s := reflect.ValueOf(v).Elem()
	typeOfT := s.Type()

	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		// Logger.Debugf("%d: %s %s = %v len=%v\n", i,
		//     typeOfT.Field(i).Name, f.Type(), f.Interface(), typeOfT.Field(i).Tag.Get("len"))
		tags := typeOfT.Field(i).Tag
		maxLen, _ := strconv.Atoi(tags.Get("len"))
		opt := tags.Get("opt")

		switch f.Kind() {

		default:
			continue

		case reflect.Int:
			if !((opt == "1") || (opt == "" && f.Interface().(int) != 0)) {
				return NewPayRet("PARAM_ERROR")
			}

		case reflect.String:
			if !(f.Len() <= maxLen && (opt == "" && f.Interface().(string) != "") || (opt == "1")) {
				return NewPayRet("PARAM_ERROR")
			}
		}
	}
	return nil
}

// http://127.0.0.1:8089/mmpaymkttransfers/promotion/transfers
func DoTransfers(w io.Writer, req *http.Request) error {
	var ack DoTransAck

	if req.Method != "POST" {
		ack.PayRet = *NewPayRet("REQUIRE_POST_METHOD")
	} else {
		data, _ := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		var v DoTransReq
		err := xml.Unmarshal(data, &v)
		if err != nil {
			ack.PayRet = *NewPayRet("XML_ERROR")
		} else {
			appData, payRet := v.returnCode()
			if payRet != nil {
				ack.PayRet = *payRet
			} else {
				if payRes := v.resultCode(appData); payRes != nil {
					ack.PayRet = *NewPayRet("SUCCESS")
					ack.PayRes = *payRes
				} else {
					indendedReturnCode := req.URL.Query().Get("indended_return_code")
					indendedResultCode := req.URL.Query().Get("indended_result_code")
					ack.PayRet = *NewPayRet(indendedReturnCode)
					if indendedReturnCode == "SUCCESS" {
						ack.AppId = NewCdataString(v.AppId)
						ack.MCHId = NewCdataString(v.MCHId)
						ack.DeviceInfo = NewCdataString(v.DeviceInfo)
						ack.NonceStr = NewCdataString(RandStr(32))
						ack.PayRes = *NewPayRes(indendedResultCode)
						if indendedResultCode == "SUCCESS" {
							ack.PaymentNo = NewCdataString(v.PartnerTradeNo)
							paymentNo := RandStr(64)
							paymentTime := time.Now().Format("2006-01-02 15:04:05")
							ack.PaymentNo = NewCdataString(paymentNo)
							ack.PaymentTime = NewCdataString(paymentTime)
						}
						key := keyGen(v.AppId, v.MCHId, v.PartnerTradeNo)
						transData := TransData{&v, &ack}
						Logger.Debugf("%v", transData)
						buffer := new(bytes.Buffer)
						encoder := gob.NewEncoder(buffer)
						err := encoder.Encode(transData)
						if err != nil {
							return err
						}
						err = DbConn.Update(func(tx *bolt.Tx) error {
							b := tx.Bucket([]byte(mmpaymkttransfersBucket))
							err := b.Put([]byte(key), buffer.Bytes())
							return err
						})
						if err != nil {
							return err
						}
					}
				}
			}
		}
	}
	xmlData, err := xml.Marshal(ack)
	if err != nil {
		return err
	}
	w.Write(xmlData)
	return nil
}

type GetTransReq struct {
	AppId          string `xml:"appid" len:"32"`
	MCHId          string `xml:"mch_id" len:"32"`
	NonceStr       string `xml:"nonce_str" len:"32"`
	Sign           string `xml:"sign" len:"32"`
	PartnerTradeNo string `xml:"partner_trade_no" len:"32"`
}

type GetTransAck struct {
	XMLName xml.Name `xml:"xml"`
	PayRet
	PayRes
	MCHId          *CdataString `xml:"mchid,omitempty"`
	PartnerTradeNo *CdataString `xml:"partner_trade_no,omitempty"`
	PaymentNo      *CdataString `xml:"detail_id,omitempty"`
	PaymentTime    *CdataString `xml:"transfer_time,omitempty"`
	Status         *CdataString `xml:"status,omitempty"`
	Reason         *CdataString `xml:"reason,omitempty"`
	OpenId         *CdataString `xml:"openid,omitempty"`
	Name           *CdataString `xml:"transfer_name,omitempty"`
	Amount         *CdataString `xml:"payment_amount,omitempty"`
	Desc           *CdataString `xml:"desc,omitempty"`
}

func (v *GetTransReq) returnCode() (*AppData, *PayRet) {
	appData, found := findAppData(v.AppId)
	if !found {
		return nil, NewPayRet("APPID_NOT_EXIST")
	}
	kvs := toMap(v)
	calcSign := wxSign(kvs, appData.MCHSecret)
	if calcSign != v.Sign {
		Logger.Debugf("client_sign %s", v.Sign)
		Logger.Debugf("server_sign %s", calcSign)
		return nil, NewPayRet("SIGN_ERROR")
	}
	return &appData, nil
}

func (v *GetTransReq) resultCode(appData *AppData) (*TransData, *PayRes) {
	var transData TransData
	key := keyGen(v.AppId, v.MCHId, v.PartnerTradeNo)
	err := DbConn.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(mmpaymkttransfersBucket))
		transDataBytes := b.Get([]byte(key))
		if transDataBytes != nil {
			dec := gob.NewDecoder(bytes.NewReader(transDataBytes))
			errGob := dec.Decode(&transData)
			return errGob
		}
		return errors.New("NOT_FOUND")
	})
	if err != nil {
		return nil, NewPayRes(err.Error())
	}
	return &transData, nil
}

func GetTransfers(w io.Writer, req *http.Request) error {
	var ack GetTransAck

	if req.Method != "POST" {
		ack.PayRet = *NewPayRet("REQUIRE_POST_METHOD")
	} else {
		data, _ := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		var v GetTransReq
		err := xml.Unmarshal(data, &v)
		if err != nil {
			ack.PayRet = *NewPayRet("XML_ERROR")
		} else {
			appData, payRet := v.returnCode()
			if payRet != nil {
				ack.PayRet = *payRet
			} else {
				transData, payRes := v.resultCode(appData)
				if payRes != nil {
					ack.PayRet = *NewPayRet("SUCCESS")
					ack.PayRes = *payRes
				} else {
					ack.PayRet = *NewPayRet("SUCCESS")
					ack.PayRes = *NewPayRes("SUCCESS")
					ack.MCHId = NewCdataString(v.MCHId)
					ack.PartnerTradeNo = NewCdataString(v.PartnerTradeNo)
					ack.PaymentNo = transData.Ack.PaymentNo
					ack.PaymentTime = transData.Ack.PaymentTime
					ack.Status = transData.Ack.ResultCode
					ack.Reason = transData.Ack.ErrCodeDes
					ack.OpenId = NewCdataString(transData.Req.OpenId)
					ack.Name = NewCdataString(transData.Req.ReUserName)
					ack.Amount = NewCdataString(strconv.Itoa(transData.Req.Amount))
					ack.Desc = NewCdataString(transData.Req.Desc)
				}
			}
		}
	}
	xmlData, err := xml.Marshal(ack)
	if err != nil {
		return err
	}
	w.Write(xmlData)
	return nil
}
