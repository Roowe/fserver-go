package utils

import (
	"io"
	"net/http"
	"text/template"
	//"errors"
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"strconv"
	"time"
)

type PayNotifyReq struct {
	XMLName       xml.Name      `xml:"xml"`
	ReturnCode    []CdataString `xml:"return_code,omitempty"`
	ReturnMsg     []CdataString `xml:"return_msg,omitempty"`
	AppId         []CdataString `xml:"appid,omitempty"`
	MCHId         []CdataString `xml:"mch_id,omitempty"`
	DeviceInfo    []CdataString `xml:"device_info,omitempty"`
	NonceStr      []CdataString `xml:"nonce_str,omitempty"`
	OpenId        []CdataString `xml:"openid,omitempty"`
	TradeType     []CdataString `xml:"trade_type,omitempty"`
	BankType      []CdataString `xml:"bank_type,omitempty"`
	TotalFee      []CdataString `xml:"total_fee,omitempty"`
	FeeType       []CdataString `xml:"fee_type,omitempty"`
	CashFee       []CdataString `xml:"cash_fee,omitempty"`
	TransactionId []CdataString `xml:"transaction_id,omitempty"`
	OutTradeNo    []CdataString `xml:"out_trade_no,omitempty"`
	Attach        []CdataString `xml:"attach,omitempty"`
	TimeEnd       []CdataString `xml:"time_end,omitempty"`
	Sign          []CdataString `xml:"sign,omitempty"`
	ResultCode    []CdataString `xml:"result_code,omitempty"`
	ErrCode       []CdataString `xml:"err_code,omitempty"`
	ErrCodeDes    []CdataString `xml:"err_code_des,omitempty"`
}

const paymentConfirmationTmpl = `
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>微信支付确认页面</title> 
  </head>
  <body>
    <div id="pay">
      <h1>商品描述: {{.GoodsDesc}}</h1>
		{{range .AccInfos}}
			<div style="padding: 5px; width: 350px; height: 64px; margin-bottom: 5px;">
			  <img style="display: inline-block; width: 64px; height: 100%; object-fit: cover;" src="{{ .HeadimgURL }}" />
			  <span >{{.Nickname}}</span>
              <button onclick="pay('{{.OpenId}}', 'SUCCESS')">支付</button>
              <button onclick="pay('{{.OpenId}}', 'FAIL', 'NOTENOUGH')">余额不足</button>
			</div>
		{{end}}
        <div style="padding: 5px; width: 350px; height: 64px; margin-bottom: 5px;">
        <button onclick="cancel()">取消</button>
        </div>
    </div>
    <div id="result">
    </div>
  </body>
  <script>
function show_result(Msg) {
    document.getElementById("pay").style.display = 'none';
    document.getElementById("result").textContent=Msg;
}
function cancel() {
    show_result("交易已取消");
}
function pay(openId, intendedResultCode, intendedErrCode) {
	var oReq = new XMLHttpRequest();
	oReq.addEventListener("load", function() {
	    console.dir(this.responseText);
	    var resp = JSON.parse(this.responseText);
	    var return_code = resp.return_code;
        var result_code = resp.result_code;
	    if (return_code == "SUCCESS" && result_code == "SUCCESS") {
		    show_result("支付成功");
	    } else {
		    show_result("支付失败，请重新扫码尝试."+this.responseText);
	    }
	});
	oReq.open("POST", "/payment/authorization");
    parms = {
        "app_id":"{{.AppId}}",
        "mch_id":"{{.MCHId}}",
        "out_trade_no":"{{.OutTradeNo}}",
        "prepay_id":"{{.PrepayId}}",
        "openid":openId,
        "intended_result_code":intendedResultCode,
        "intended_err_code":intendedErrCode
    },
    oReq.send(JSON.stringify(parms));
}

	</script>
</html>
`

var paymentConfirmationT = template.Must(template.New("payment_confirmation").Parse(paymentConfirmationTmpl))

func PaymentConfirmationHandle(w io.Writer, req *http.Request) error {
	appId := req.URL.Query().Get("appid")
	MCHId := req.URL.Query().Get("mch_id")
	outTradeNo := req.URL.Query().Get("out_trade_no")
	prepayId := req.URL.Query().Get("prepay_id")
	appData, found := findAppData(appId)
	var reply []byte
	if found {
		key := keyGen(appId, MCHId, outTradeNo)
		ppI, found := prepayCache.Get(key)
		if found {
			pp := ppI.(Prepay)
			paymentConfirmationT.Execute(w, map[string]interface{}{
				"GoodsDesc":  pp.Req.Body,
				"AppId":      appId,
				"MCHId":      MCHId,
				"OutTradeNo": outTradeNo,
				"PrepayId":   prepayId,
				"AccInfos":   appData.AccInfos,
			})
			return nil
		} else {
			reply = []byte(MSG_ERROR_PAYMENT_CLOSED)
		}
	} else {
		reply = []byte(MSG_ERROR_APP_ID)
	}
	w.Write(reply)
	return nil
}

func PaymentAuthorizationHandle(w io.Writer, req *http.Request) error {
	data, _ := ioutil.ReadAll(req.Body)
	req.Body.Close()
	kvs := map[string]string{
		"intended_return_code": "SUCCESS",
	}
	err := json.Unmarshal(data, &kvs)
	if err != nil {
		return err
	}
	Logger.Debugf("kvs %v", kvs)

	appId := kvs["app_id"]
	MCHId := kvs["mch_id"]
	outTradeNo := kvs["out_trade_no"]
	prepayId := kvs["prepay_id"]

	resultCode := kvs["intended_result_code"]
	reply := map[string]string{
		"return_code": kvs["intended_return_code"],
		"result_code": "FAIL",
		"err_code":    "ORDERCLOSED",
	}
	key := keyGen(appId, MCHId, outTradeNo)
	ppI, found := prepayCache.Get(key)
	if found {
		pp := ppI.(Prepay)
		if resultCode == "SUCCESS" &&
			pp.Timestamp+2*3600 >= time.Now().Unix() &&
			pp.PrepayId == prepayId {
			prepayCache.Delete(key)
		}
		reply = map[string]string{
			"return_code": kvs["intended_return_code"],
			"result_code": kvs["intended_result_code"],
			"err_code":    kvs["intended_err_code"],
		}
		t := time.Now()
		payNotifyReq := PayNotifyReq{
			ReturnCode:    CdataStrings(kvs["intended_return_code"]),
			ResultCode:    CdataStrings(kvs["intended_result_code"]),
			ErrCode:       CdataStrings(kvs["intended_err_code"]),
			AppId:         CdataStrings(pp.Req.AppId),
			MCHId:         CdataStrings(pp.Req.MCHId),
			DeviceInfo:    CdataStrings(pp.Req.DeviceInfo),
			NonceStr:      CdataStrings(RandStr(32)),
			OpenId:        CdataStrings(kvs["openid"]),
			TradeType:     CdataStrings(pp.Req.TradeType),
			BankType:      CdataStrings("CMC"),
			TotalFee:      CdataStrings(strconv.Itoa(pp.Req.TotalFee)),
			FeeType:       CdataStrings("CNY"),
			CashFee:       CdataStrings("0"),
			TransactionId: CdataStrings(RandStr(32)),
			OutTradeNo:    CdataStrings(pp.Req.OutTradeNo),
			Attach:        CdataStrings(pp.Req.Attach),
			TimeEnd:       CdataStrings(t.Format("20091225091010")),
		}
		appData, found := findAppData(appId)
		if found {
			calcSign := wxSign(toMap(&payNotifyReq), appData.MCHSecret)
			payNotifyReq.Sign = CdataStrings(calcSign)
			xmlData, _ := xml.Marshal(payNotifyReq)
			go payNotifyLoop(pp.Req.NotifyURL, xmlData)
		}
	}
	replyBody, _ := json.Marshal(reply)
	w.Write(replyBody)
	return nil
}
