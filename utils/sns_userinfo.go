package utils

import (
	"encoding/json"
	"io"
	"net/http"
)

// http://127.0.0.1:8089/sns/userinfo?appid=app_id_1&access_token=access_token&openid=openid
func SnsUserinfoHandle(w io.Writer, req *http.Request) error {
	appId := req.URL.Query().Get("appid")
	accessToken := req.URL.Query().Get("access_token")
	openId := req.URL.Query().Get("openid")
	var ret int
	appData, found := findAppData(appId)
	var accRet Account
	if found {
		ret = EC_ERROR_PARAMS
		for _, acc := range appData.AccInfos {
			if acc.AccessToken == accessToken &&
				acc.OpenId == openId {
				accRet = acc
				ret = 0
				break
			}
		}
	} else {
		ret = EC_ERROR_APP_ID
	}

	data := make(map[string]interface{})

	if ret != 0 {
		data["ret"] = ret
	} else {
		data["nickname"] = accRet.Nickname
		data["headimgurl"] = accRet.HeadimgURL
	}
	reply, _ := json.Marshal(data)
	w.Write(reply)
	return nil
}
