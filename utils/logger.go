package utils

import (
	"github.com/op/go-logging"
	"os"
)

var Logger = logging.MustGetLogger("fserver")

func InitLogger(debug bool) {
	format := logging.MustStringFormatter(
		`%{color}%{time:06-01-02 15:04:05.000} %{level:.5s} @%{shortfile}%{color:reset} %{message}`,
	)
	logging.SetFormatter(format)
	logging.SetBackend(logging.NewLogBackend(os.Stdout, "", 0))

	if debug {
		logging.SetLevel(logging.DEBUG, "fserver")
	} else {
		logging.SetLevel(logging.INFO, "fserver")
	}
}

func GetLogger() *logging.Logger {
	return Logger
}
