package utils

import (
	"encoding/json"
	"io"
	"net/http"
)

// http://127.0.0.1:8089/auth/fake/acc/list?appid=app_id_1
func AuthFakeAccListHandle(w io.Writer, req *http.Request) error {
	appId := req.URL.Query().Get("appid")
	var ret int = 0
	appData, found := findAppData(appId)
	accList := make([]map[string]interface{}, len(appData.AccInfos))
	if found {
		for i, acc := range appData.AccInfos {
			acc := map[string]interface{}{
				"id":         acc.FakeAccId,
				"code":       acc.Code,
				"nickname":   acc.Nickname,
				"headimgurl": acc.HeadimgURL,
			}
			accList[i] = acc
		}
	} else {
		ret = EC_ERROR_APP_ID
	}
	data := make(map[string]interface{})

	if ret != 0 {
		data["ret"] = ret
	} else {
		data["data"] = accList
	}
	reply, _ := json.Marshal(data)
	w.Write(reply)
	return nil
}
