package utils

import (
	"encoding/json"
	"io"
	"net/http"
	"text/template"
)

const connectOauth2AuthTmpl = `
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FakeOAuth2Relay</title> 
  </head>
  <body>    
		{{range .AccInfos}}
			<div style="padding: 5px; width: 350px; height: 64px; margin-bottom: 5px;">
				<img style="display: inline-block; width: 64px; height: 100%; object-fit: cover;" src="{{ .HeadimgURL }}" />
				<button style="vertical-align: top; display: inline-block; margin-left: 5px; width: 250px; height: 100%; cursor: pointer;" onclick="invokeApi2('{{.FakeAccId}}')">{{.Nickname}}</button>
			</div>
		{{end}}
  </body>
	<script>
		function invokeApi2(fakeAccountId) {
			var oReq = new XMLHttpRequest();
			oReq.addEventListener("load", function() {
				console.dir(this.responseText);
				var resp = JSON.parse(this.responseText); 
				var code = resp.code;
				if (!code) {
					alert("Invalid code returned!");
				} else {
					window.location.assign("{{.RedirectURI}}" + "?code=" + encodeURIComponent(code) + "&state=" + encodeURIComponent("{{.State}}"));
				}
			});
			oReq.open("GET", "/auth/fake/login?fake_acc_id=" + encodeURIComponent(fakeAccountId) + "&appid=" + encodeURIComponent("{{.AppId}}"));
			oReq.send();
		}

	</script>
</html>
`

var connectOauth2AuthT = template.Must(template.New("connect_oauth2_authorize").Parse(connectOauth2AuthTmpl))

// http://127.0.0.1:8089/connect/oauth2/authorize?appid=app_id_1&connect_redirect=1&redirect_uri=http%3A%2F%2Fstaging.red0769.com%3A8001%2Fnode%2Fsync-cb%2Fwechat-pubsrv-login&response_type=code&scope=snsapi_user&state=dummy_var%3D1
func ConnectOauth2AuthHandle(w io.Writer, req *http.Request) error {
	appId := req.URL.Query().Get("appid")
	redirectURI := req.URL.Query().Get("redirect_uri")
	state := req.URL.Query().Get("state")

	appData, found := findAppData(appId)
	if found {
		connectOauth2AuthT.Execute(w, map[string]interface{}{
			"RedirectURI": redirectURI,
			"State":       state,
			"AppId":       appId,
			"AccInfos":    appData.AccInfos,
		})
	} else {
		data := map[string]interface{}{
			"ret": EC_ERROR_APP_ID,
		}
		reply, _ := json.Marshal(data)
		w.Write(reply)
	}
	return nil
}
