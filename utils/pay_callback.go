package utils

import (
	"io"
	"io/ioutil"
	"net/http"
)

const payCallbackXML = `
<xml>
  <return_code><![CDATA[SUCCESS]]></return_code>
  <return_msg><![CDATA[OK]]></return_msg>
</xml>
`

func PayCallbackHandle(w io.Writer, req *http.Request) error {
	data, _ := ioutil.ReadAll(req.Body)
	req.Body.Close()
	Logger.Debugf("pay_notify body: %s", data)
	w.Write([]byte(payCallbackXML))
	return nil
}
