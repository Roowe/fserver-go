package utils

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

var DataFile string
var GAppDatas AppDatas

type AppDatas []AppData

type AppData struct {
	AppId     string    `json:"app_id"`
	Secret    string    `json:"secret"`
	MCHId     string    `json:"mch_id"`
	MCHSecret string    `json:"mch_secret"`
	AccInfos  []Account `json:"acc_info"`
}

type Account struct {
	FakeAccId   string `json:"fake_acc_id"`
	OpenId      string `json:"openid"`
	UnionId     string `json:"unionid"`
	Nickname    string `json:"nickname"`
	HeadimgURL  string `json:"headimgurl"`
	Code        string `json:"code"`
	AccessToken string `json:"access_token"`
}

func loadData(filename string) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		Logger.Errorf("readfile %s, error %s", filename, err)
		os.Exit(1)
	}
	json.Unmarshal(data, &GAppDatas)
}

func findAppData(appId string) (AppData, bool) {
	loadData(DataFile)
	var ret AppData
	for _, appData := range GAppDatas {
		if appData.AppId == appId {
			ret = appData
			return ret, true
		}
	}
	return ret, false
}
