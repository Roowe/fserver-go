package utils

import (
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"encoding/xml"
	"errors"
	"github.com/patrickmn/go-cache"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

// http://127.0.0.1:8089/pay/unifiedorder

type PayUnifiedorderReq struct {
	AppId          string `xml:"appid" len:"32"`
	MCHId          string `xml:"mch_id" len:"32"`
	DeviceInfo     string `xml:"device_info" len:"32" opt:"1"`
	NonceStr       string `xml:"nonce_str" len:"32"`
	Sign           string `xml:"sign" len:"32"`
	SignType       string `xml:"sign_type" len:"32" opt:"1"`
	Body           string `xml:"body" len:"128"`
	Detail         string `xml:"detail" len:"6000" opt:"1"`
	Attach         string `xml:"attach" len:"127" opt:"1"`
	OutTradeNo     string `xml:"out_trade_no" len:"32"`
	FeeType        string `xml:"fee_type" len:"16" opt:"1"`
	TotalFee       int    `xml:"total_fee"`
	SpbillCreateIP string `xml:"spbill_create_ip" len:"16"`
	TimeStart      string `xml:"time_start" len:"14" opt:"1"`
	TimeExpire     string `xml:"time_expire" len:"14" opt:"1"`
	GoodsTag       string `xml:"goods_tag" len:"32" opt:"1"`
	NotifyURL      string `xml:"notify_url" len:"256"`
	TradeType      string `xml:"trade_type" len:"16"`
	ProductId      string `xml:"product_id" len:"32" opt:"1"`
	LimitPay       string `xml:"limit_pay" len:"32" opt:"1"`
	OpenId         string `xml:"open_id" len:"128" opt:"1"`
	SceneInfo      string `xml:"scene_info" len:"256" opt:"1"`
}
type CdataString struct {
	//XMLName xml.Name `xml:"return_code"`
	V string `xml:",cdata"`
}
type PayUnifiedorderAck struct {
	XMLName    xml.Name      `xml:"xml"`
	ReturnCode []CdataString `xml:"return_code,omitempty"`
	ReturnMsg  []CdataString `xml:"return_msg,omitempty"`
	AppId      []CdataString `xml:"appid,omitempty"`
	MCHId      []CdataString `xml:"mch_id,omitempty"`
	DeviceInfo []CdataString `xml:"device_info,omitempty"`
	NonceStr   []CdataString `xml:"nonce_str,omitempty"`
	Sign       []CdataString `xml:"sign,omitempty"`
	ResultCode []CdataString `xml:"result_code,omitempty"`
	ErrCode    []CdataString `xml:"err_code,omitempty"`
	ErrCodeDes []CdataString `xml:"err_code_des,omitempty"`
	TradeType  []CdataString `xml:"trade_type,omitempty"`
	PrepayId   []CdataString `xml:"prepay_id,omitempty"`
	CodeURL    []CdataString `xml:"code_url,omitempty"`
}

type Prepay struct {
	Req       PayUnifiedorderReq
	PrepayId  string
	Timestamp int64
}

func CdataStrings(s string) []CdataString {
	return []CdataString{CdataString{s}}
}
func NewCdataString(s string) *CdataString {
	return &CdataString{s}
}

var prepayCache = cache.New(120*time.Minute, 15*time.Minute)
var codeDesc = map[string]string{
	"NOT_SUPPORT_FUNC":      "尚未支持的功能",
	"SUCCESS":               "SUCCESS",
	"NOAUTH":                "商户无此接口权限",
	"NOTENOUGH":             "余额不足",
	"ORDERPAID":             "商户订单已支付",
	"ORDERCLOSED":           "订单已关闭",
	"SYSTEMERROR":           "系统错误",
	"APPID_NOT_EXIST":       "APPID不存在",
	"MCHID_NOT_EXIST":       "MCHID不存在",
	"APPID_MCHID_NOT_MATCH": "appid和mch_id不匹配",
	"LACK_PARAMS":           "缺少参数",
	"OUT_TRADE_NO_USED":     "商户订单号重复",
	"SIGNERROR":             "签名错误",
	"XML_FORMAT_ERROR":      "XML格式错误",
	"REQUIRE_POST_METHOD":   "请使用post方法",
	"POST_DATA_EMPTY":       "post数据为空",
	"NOT_UTF8":              "编码格式错误",
	"OUT_TRADE_NO_ERR":      "订单号格式错误",
}

func fail_return_code(errCode string) PayUnifiedorderAck {
	return PayUnifiedorderAck{
		ReturnCode: CdataStrings("FAIL"),
		ReturnMsg:  CdataStrings(codeDesc[errCode]),
	}
}

func PayUnifiedorderHandle(w io.Writer, req *http.Request) error {

	var ack PayUnifiedorderAck
	if req.Method == "POST" {
		data, _ := ioutil.ReadAll(req.Body)
		req.Body.Close()
		var v PayUnifiedorderReq
		err := xml.Unmarshal([]byte(data), &v)
		if err != nil {
			return err
		}
		ack = v.handle()
	} else {
		ack = fail_return_code("REQUIRE_POST_METHOD")
	}
	xmlData, err := xml.Marshal(ack)
	if err != nil {
		return err
	}
	w.Write(xmlData)
	return nil
}
func (v *PayUnifiedorderReq) handle() PayUnifiedorderAck {
	appData, err := v.return_code()
	if err != nil {
		return fail_return_code(err.Error())
	}
	ack := PayUnifiedorderAck{
		ReturnCode: CdataStrings("SUCCESS"),
		ReturnMsg:  CdataStrings(codeDesc["SUCCESS"]),
		AppId:      CdataStrings(v.AppId),
		MCHId:      CdataStrings(v.MCHId),
		DeviceInfo: CdataStrings(v.DeviceInfo),
		NonceStr:   CdataStrings(RandStr(32)),
	}
	err = v.result_code(appData)
	if err != nil {
		ack.ResultCode = CdataStrings("FAIL")
		ack.ErrCode = CdataStrings(err.Error())
		ack.ErrCodeDes = CdataStrings(codeDesc[err.Error()])
	} else {
		ack.ResultCode = CdataStrings("SUCCESS")
		v.prepay(appData, &ack)
	}
	ackSign := wxSign(toMap(&ack), appData.MCHSecret)
	Logger.Debugf("ackSign %s", ackSign)
	ack.Sign = CdataStrings(ackSign)
	return ack
}

func (v *PayUnifiedorderReq) return_code() (*AppData, error) {
	err := v.verifyFieldType()
	if err != nil {
		return nil, err
	}
	appData, found := findAppData(v.AppId)
	if !found {
		return nil, errors.New("APPID_NOT_EXIST")
	}
	kvs := toMap(v)
	calcSign := wxSign(kvs, appData.MCHSecret)
	if calcSign != v.Sign {
		Logger.Debugf("client_sign %s", v.Sign)
		Logger.Debugf("server_sign %s", calcSign)
		return nil, errors.New("SIGNERROR")
	}
	return &appData, nil
}
func (v *PayUnifiedorderReq) result_code(appData *AppData) error {
	if appData.MCHId != v.MCHId {
		return errors.New("MCHID_NOT_EXIST")
	}
	if v.TradeType != "NATIVE" {
		return errors.New("NOT_SUPPORT_FUNC")
	}
	if m, _ := regexp.MatchString("^[a-zA-Z0-9_-]{0,32}$", v.OutTradeNo); !m {
		return errors.New("OUT_TRADE_NO_ERR")
	}
	key := keyGen(v.AppId, v.MCHId, v.OutTradeNo)
	pp, found := prepayCache.Get(key)
	if found {
		Logger.Debugf("prepay %v", pp)
		return errors.New("OUT_TRADE_NO_USED")
	}
	return nil
}
func (v *PayUnifiedorderReq) prepay(appData *AppData, ack *PayUnifiedorderAck) {

	key := keyGen(v.AppId, v.MCHId, v.OutTradeNo)
	prepayId := RandStr(64)
	pp := Prepay{
		Req:       *v,
		PrepayId:  prepayId,
		Timestamp: time.Now().Unix(),
	}
	prepayCache.Set(key, pp, cache.NoExpiration)
	ack.PrepayId = CdataStrings(prepayId)
	ack.TradeType = CdataStrings(v.TradeType)
	ack.CodeURL = CdataStrings(makeCodeUrl(*v, prepayId))
}
func (v *PayUnifiedorderReq) verifyFieldType() error {
	s := reflect.ValueOf(v).Elem()
	typeOfT := s.Type()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		// fmt.Printf("%d: %s %s = %v len=%v\n", i,
		//     typeOfT.Field(i).Name, f.Type(), f.Interface(), typeOfT.Field(i).Tag.Get("len"))
		tags := typeOfT.Field(i).Tag
		maxLen, _ := strconv.Atoi(tags.Get("len"))
		opt := tags.Get("opt")

		switch f.Kind() {

		default:
			continue

		case reflect.Int:
			if !((opt == "1") || (opt == "" && f.Interface().(int) != 0)) {
				return errors.New("LACK_PARAMS")
			}

		case reflect.String:
			if !(f.Len() <= maxLen && (opt == "" && f.Interface().(string) != "") || (opt == "1")) {
				return errors.New("LACK_PARAMS")
			}
		}
	}
	return nil
}

func toMap(v interface{}) map[string]string {
	s := reflect.ValueOf(v).Elem()
	typeOfT := s.Type()
	kvs := make(map[string]string)
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		// fmt.Printf("%d: %s %s = %v len=%v\n", i,
		//     typeOfT.Field(i).Name, f.Type(), f.Interface(), typeOfT.Field(i).Tag.Get("len"))
		tags := typeOfT.Field(i).Tag
		xmlTag := tags.Get("xml")
		xmlField := strings.Split(xmlTag, ",")[0]
		var val string
		switch f.Kind() {
		default:
			continue
		case reflect.Slice:
			if f.Len() > 0 {
				val = f.Index(0).Interface().(CdataString).V
			}

		case reflect.Int:
			vInt := f.Interface().(int)
			if vInt != 0 {
				val = strconv.Itoa(vInt)
			}

		case reflect.String:
			val = f.Interface().(string)
		}
		if xmlField != "sign" && val != "" {
			kvs[xmlField] = val
		}
	}
	return kvs
}

func keyGen(appId string, MCHId string, outTradeNo string) string {
	Logger.Debugf("AppId %s, MCHId %s, OutTradeno %s", appId, MCHId, outTradeNo)
	bytes := md5.Sum([]byte(appId + MCHId + outTradeNo))
	key := hex.EncodeToString(bytes[:])
	Logger.Debugf("key %s", key)
	return key
}

func RandStr(len int) string {
	b := make([]byte, len/2)
	rand.Read(b)
	return hex.EncodeToString(b)
}

func wxSign(kvs map[string]string, secret string) string {
	//Logger.Debug(kvs)
	ks := make([]string, 0, len(kvs))
	for k := range kvs {
		ks = append(ks, k)
	}
	sort.Strings(ks)
	var buf bytes.Buffer
	for _, k := range ks {
		buf.WriteString(k + "=" + kvs[k] + "&")
	}
	buf.WriteString("key=" + secret)
	Logger.Debug("sign string:", buf.String())
	md5Bytes := md5.Sum(buf.Bytes())
	return strings.ToUpper(hex.EncodeToString(md5Bytes[:]))
}

func makeCodeUrl(v PayUnifiedorderReq, prepayId string) string {
	urlV := url.Values{
		"appid":        {v.AppId},
		"mch_id":       {v.MCHId},
		"out_trade_no": {v.OutTradeNo},
		"prepay_id":    {prepayId},
	}
	URL := "http://" + Domain + "/payment/confirmation?" + urlV.Encode()
	Logger.Debugf("url : %s", URL)
	return URL
}
