package utils

import (
	"encoding/json"
	"io"
	"net/http"
)

// http://127.0.0.1:8089/auth/fake/login?fake_acc_id=111&appid=app_id_1
func AuthFakeLoginHandle(w io.Writer, req *http.Request) error {
	accId := req.URL.Query().Get("fake_acc_id")
	appId := req.URL.Query().Get("appid")

	var accRet Account
	var ret int
	appData, found := findAppData(appId)
	if found {
		ret = EC_ERROR_ACC_ID
		for _, acc := range appData.AccInfos {
			if acc.FakeAccId == accId {
				accRet = acc
				ret = 0
				break
			}
		}
	} else {
		ret = EC_ERROR_APP_ID
	}
	data := make(map[string]interface{})

	if ret != 0 {
		data["ret"] = ret
	} else {
		data["code"] = accRet.Code
	}
	reply, _ := json.Marshal(data)
	w.Write(reply)
	return nil
}
