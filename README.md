## 安装golang

版本要求 1.11或以上

https://golang.org/doc/install
## 下载编译好的二进制【推荐】
Linux 64bit
地址是 https://rdl.oss-cn-hongkong.aliyuncs.com/dl/fserver.tar.gz

## Compiling and running in test mode

如果想自己编译或者不是Linux 64bit的环境，就继续看这部分文档

将本仓库mv到GOPATH，默认是$HOME/go

```
user@proj-root> go run ./main.go
```
