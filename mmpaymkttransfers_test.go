package main

import (
	"bytes"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestDoTransfers(t *testing.T) {
	v := DoTransReq{
		AppId:          "wx2421b1c4370ec43b",
		MCHId:          "10000100",
		DeviceInfo:     "",
		NonceStr:       RandStr(32),
		PartnerTradeNo: RandStr(32),
		OpenId:         "oEkp8wXtRIIDnTce8Ys_uMp_UP94",
		CheckName:      "FORCE_CHECK",
		ReUserName:     "张三",
		Amount:         100,
		Desc:           "节日快乐",
		SpBillCreateIp: "10.2.1.10",
	}
	kvs := toMap(&v)
	calcSign := wxSign(kvs, "mch_secret")
	v.Sign = calcSign
	xmlData, err := xml.Marshal(v)
	if err != nil {
		t.Fatal("xml failed")
	}
	resp, err := http.Post("http://127.0.0.1:8089/mmpaymkttransfers/promotion/transfers?indended_return_code=SUCCESS&indended_result_code=SUCCESS",
		"application/xml", bytes.NewReader(xmlData))

	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(body))
}
