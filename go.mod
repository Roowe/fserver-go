module fserver

require (
	github.com/boltdb/bolt v1.3.1
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/patrickmn/go-cache v2.1.0+incompatible
)
